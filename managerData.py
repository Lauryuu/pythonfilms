from mysql import connector
from dotenv import load_dotenv
from os import getenv


class Manager:
    load_dotenv()

    def __init__(self):
        self.db_film = connector.connect(
            host="localhost",
            database=getenv("DB_NAME"),
            user=getenv("DB_USER"),
            password=getenv("DB_PASS")
        )
        self.cursor = self.db_film.cursor()

    def search_films(self, title_search):
        query = "SELECT * FROM films WHERE title LIKE %(title_search)s OR original_title LIKE %(title_search)s"
        self.cursor.execute(query, {"title_search": "%" + title_search + "%"})
        return self.cursor.fetchall()

    def get_info_film(self, id_film):
        query = "SELECT * FROM films WHERE id = %(id)s"
        self.cursor.execute(query, {"id": id_film})
        return self.cursor.fetchone()


m = Manager()
m.get_info_film(348)
