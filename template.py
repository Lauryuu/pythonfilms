import tkinter
import managerData


class Template:
    def __init__(self):
        self.root = tkinter.Tk()
        self.root.geometry("1000x5000")
        self.root.config(bg="#191970")

        # self.scroll = tkinter.Scrollbar(self.root)
        # self.scroll.pack(side='right', fill="y")
        # self.scroll.grid()
        # self.mylist = tkinter.Listbox(self.root, yscrollcommand=self.scroll.set)

        self.search = tkinter.Entry(self.root, width='50', bg="#C0C0C0")
        self.search.grid()
        self.search.bind("<KeyPress-Return>", self.display_films)

        self.root.mainloop()

    def display_films(self, e):
        response = self.search.get()
        manager = managerData.Manager()
        data = manager.search_films(response)
        for d in data:
            film = tkinter.Label(self.root, text=d[2], bg="#191970", fg="#f04d45", font=("Courier", 20))
            film.grid()
            film.bind("<Button-1>", self.display_info)

    def display_info(self, e):
        print(e)
        # manager = managerData.Manager()
        # infos = manager.get_info_film()


template = Template()
